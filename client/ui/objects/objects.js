const STATE = {
  OBJECTS: 'OBJECTS',
  VEHICLES: 'VEHICLES',
  WEAPONS: 'WEAPONS',
  DOORS: 'DOORS',
  CLOTHING: 'CLOTHING',
  SPECIAL: 'SPECIAL'
};

let state = {
  state : STATE.OBJECTS,
  objectLoaded : false,
  vehicleLoaded : false,
  weaponLoaded : false,
  doorLoaded : false,
  clothingLoaded : false,
  specialObjectsLoaded : false,
};

function SetState(to) {
  let from = state.state;

  let arr = ['objects', 'vehicles', 'weapons', 'doors', 'clothing', 'special'];
  for (let i = 0; i < arr.length; i++) {
    document.getElementById(arr[i]).classList.add('hidden');
    document.getElementById(`tab-${arr[i]}`).classList.remove('active');
  }

  switch (to) {
    case STATE.OBJECTS:
      document.getElementById('objects').classList.remove('hidden');
      document.getElementById('tab-objects').classList.add('active');
      break;
    case STATE.VEHICLES:
      document.getElementById('vehicles').classList.remove('hidden');
      document.getElementById('tab-vehicles').classList.add('active');
      break;
    case STATE.WEAPONS:
      document.getElementById('weapons').classList.remove('hidden');
      document.getElementById('tab-weapons').classList.add('active');
      break;
    case STATE.DOORS:
      document.getElementById('doors').classList.remove('hidden');
      document.getElementById('tab-doors').classList.add('active');
      break;
    case STATE.CLOTHING:
      document.getElementById('clothing').classList.remove('hidden');
      document.getElementById('tab-clothing').classList.add('active');
      break;
    case STATE.SPECIAL:
      document.getElementById('special').classList.remove('hidden');
      document.getElementById('tab-special').classList.add('active');
      break;
  }

  state.state = to;
}

OnDocumentReady(function() {
  document.getElementById('tab-objects').onclick = function() {
    SetState(STATE.OBJECTS);
  };

  document.getElementById('tab-vehicles').onclick = function() {
    SetState(STATE.VEHICLES);
  };

  document.getElementById('tab-weapons').onclick = function() {
    SetState(STATE.WEAPONS);
  };

  document.getElementById('tab-doors').onclick = function() {
    SetState(STATE.DOORS);
  };

  document.getElementById('tab-clothing').onclick = function() {
    SetState(STATE.CLOTHING);
  };

  document.getElementById('tab-special').onclick = function() {
    SetState(STATE.SPECIAL);
  };
});

function Load(objectCount, vehicleCount, weaponCount, clothingCount, doorCount) {
  LoadObjects(objectCount);
  LoadVehicles(vehicleCount);
  LoadWeapons(weaponCount);
  LoadDoors(doorCount);
  LoadClothing(clothingCount);
  LoadSpecialObjects();
}

function LoadObjects(amount) {
  if (state.objectLoaded) return;
  state.objectLoaded = true;

  let listbox = document.getElementById('objects');

  let appendHTML = '';
  for (let i = 1; i < amount + 1; i++) {
    appendHTML += `<div class="item" data-id="${i}">
      <img src="http://game/objects/${i}" />
      <div class="top-left">${i}</div>
    </div>`;
  }
  listbox.innerHTML += appendHTML;

  let nodes = listbox.getElementsByClassName('item');
  for (let i = 0; i < nodes.length; i++) {
    let node = nodes[i];

    node.onclick = function() {
      CallEvent('CreateObjectPlacement', this.dataset.id);
    };
  }
}

function LoadVehicles(amount) {
  if (state.vehicleLoaded) return;
  state.vehicleLoaded = true;

  let listbox = document.getElementById('vehicles');

  let appendHTML = '';
  for (let i = 1; i < amount + 1; i++) {
    let modelID = 0;
    for (let key in VEHICLE_CONFIG) {
      let vehicleCfg = VEHICLE_CONFIG[key];

      if (vehicleCfg.vehicleID == i) {
        modelID = vehicleCfg.modelID;
        break;
      }
    }

    appendHTML += `<div class="item" data-id="${i}">
      <img src="http://game/objects/${modelID}" />
      <div class="top-left">${i}</div>
    </div>`;
  }
  listbox.innerHTML += appendHTML;

  let nodes = listbox.getElementsByClassName('item');
  for (let i = 0; i < nodes.length; i++) {
    let node = nodes[i];

    node.onclick = function() {
      CallEvent('CreateVehiclePlacement', this.dataset.id);
    };
  }
}

function LoadWeapons(amount) {
  if (state.weaponLoaded) return;
  state.weaponLoaded = true;

  let listbox = document.getElementById('weapons');

  let appendHTML = '';
  for (let i = 2; i < amount + 1; i++) {
    let modelID = 0;
    for (let key in WEAPON_CONFIG) {
      let weaponCfg = WEAPON_CONFIG[key];
      
      if (weaponCfg.weaponID == i) {
        modelID = weaponCfg.modelID;
        break;
      }
    }

    appendHTML += `<div class="item" data-objectid="${modelID}" data-weaponid="${i}">
      <img src="http://game/objects/${modelID}" />
      <div class="top-left">${i}</div>
    </div>`;
  }
  listbox.innerHTML += appendHTML;

  let nodes = listbox.getElementsByClassName('item');
  for (let i = 0; i < nodes.length; i++) {
    let node = nodes[i];

    node.onclick = function() {
      CallEvent('CreateWeaponPlacement', this.dataset.objectid, this.dataset.weaponid);
    };
  }
}

function LoadDoors(amount) {
  if (state.doorLoaded) return;
  state.doorLoaded = true;

  let listbox = document.getElementById('doors');

  let appendHTML = '';
  for (let i = 1; i < amount + 1; i++) {
    let modelID = 0;
    let isCustom = false;

    for (let key in DOOR_CONFIG) {
      let doorCfg = DOOR_CONFIG[key];
      
      if (doorCfg.doorID == i) {
        modelID = doorCfg.modelID;
        isCustom = doorCfg.pictureID != null;
        break;
      }
    }

    appendHTML += `<div class="item" data-objectid="${modelID}" data-doorid="${i}">
      <img src="${(isCustom ? `http://asset/sandbox/client/ui/common/doors/${i}.jpg` : `http://game/objects/${modelID}`)}" />
      <div class="top-left">${i}</div>
    </div>`;
  }
  listbox.innerHTML += appendHTML;

  let nodes = listbox.getElementsByClassName('item');
  for (let i = 0; i < nodes.length; i++) {
    let node = nodes[i];

    node.onclick = function() {
      CallEvent('CreateDoorPlacement', this.dataset.objectid, this.dataset.doorid);
    };
  }
}

function LoadClothing(amount) {
  if (state.clothingLoaded) return;
  state.clothingLoaded = true;

  let listbox = document.getElementById('clothing');

  let appendHTML = '';
  for (let i = 1; i < amount + 1; i++) {
    appendHTML += `<div class="item" data-id="${i}">
      <img src="http://asset/sandbox/client/ui/common/clothing/${(i == 11 ? 10 : i)}.jpg" />
      <div class="top-left">${i}</div>
    </div>`;
  }
  listbox.innerHTML += appendHTML;

  let nodes = listbox.getElementsByClassName('item');
  for (let i = 0; i < nodes.length; i++) {
    let node = nodes[i];

    node.onclick = function() {
      CallEvent('RequestClothingPreset', this.dataset.id);
    };
  }
}

// Special objects
function LoadSpecialObjects() {
  if (state.specialObjectsLoaded) return;
  state.specialObjectsLoaded = true;

  let listbox = document.getElementById('special');

  let appendHTML = '';
  for (const key in SPECIAL_OBJECTS_CONFIG) {
    const config = SPECIAL_OBJECTS_CONFIG[key];
    appendHTML += `<b>${key}</b>`;
    let i = 0;
    for (const modelId of config.models) {
      appendHTML += `<div class="item" data-model="${modelId}" data-type="${key}">
          <img src="http://game/objects/${modelId}" />
          <div class="top-left">${key}-${i}</div>
        </div>`;
      i += 1;
    }
    appendHTML += `<hr><br />`
  }
  listbox.innerHTML += appendHTML;

  let nodes = listbox.getElementsByClassName('item');
  for (let i = 0; i < nodes.length; i++) {
    let node = nodes[i];

    node.onclick = function() {
      const data = SPECIAL_OBJECTS_CONFIG[this.dataset.type].data;
      let jsonData;
      if (data) {
        jsonData = JSON.stringify(data);
      }
      CallEvent('CreateSpecialObjectPlacement', this.dataset.model, this.dataset.type, jsonData);
    };
  }
}
