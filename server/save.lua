EditorObjects = {}
EditorDoors = {}
EditorSchematics = {}

local function Editor_SaveWorld()
  local _table = {}

  -- Save objects
  for _,v in pairs(EditorObjects) do
    local _object = {}

    local x, y, z = GetObjectLocation(v)
    local rx, ry, rz = GetObjectRotation(v)
    local sx, sy, sz = GetObjectScale(v)

    _object['modelID'] = GetObjectModel(v)
    _object['x'] = x
    _object['y'] = y
    _object['z'] = z

    _object['rx'] = rx
    _object['ry'] = ry
    _object['rz'] = rz

    _object['sx'] = sx
    _object['sy'] = sy
    _object['sz'] = sz

    -- Special objects
    local data = rawget(EditorSpecialObjectData, v)
    if data then
      _object['specialObjectData'] = data
    end
  
    table.insert(_table, _object)
  end

  -- Save doors
  for _,v in pairs(EditorDoors) do
    local _door = {}

    local x, y, z = GetDoorLocation(v)
    local yaw = EditorDoorData[v]['yaw']
    if yaw == nil then
      yaw = 0
    end

    _door['doorID'] = GetDoorModel(v)
    _door['x'] = x
    _door['y'] = y
    _door['z'] = z
    _door['yaw'] = yaw
  
    table.insert(_table, _door)
  end
  
  File_SaveJSONTable('world.json', _table)
  AddPlayerChatAll('Server: Saved the world.')
end
CreateTimer(Editor_SaveWorld, 10 * 60 * 1000)
AddCommand('save', Editor_SaveWorld)
AddRemoteEvent('WorldSave', Editor_SaveWorld)

-- Custom world loader
AddEvent("WorldObjectLoaded", function(objectId, data)
  if data['modelID'] ~= nil then
    Editor_CreateObject(nil, data['modelID'], data['x'], data['y'], data['z'],
            data['rx'], data['ry'], data['rz'], data['sx'], data['sy'], data['sz'], objectId)
    print("EditorLoadObject(".. data['modelID']..", ".. data['x']..", ".. data['y']..", ".. data['z']..", ".. data['rx']
            ..", ".. data['ry']..", ".. data['rz']..", ".. data['sx']..", ".. data['sy']..", ".. data['sz']..")")
    if data['specialObjectData'] then
      EditorSpecialObjectData[objectId] = data['specialObjectData']
      print("EditorLoadSpacialObject "..json_encode(data['specialObjectData']))
    end
  else
    Editor_CreateDoor(data['doorID'], data['x'], data['y'], data['z'], data['yaw'], objectId)
    print("EditorLoadDoor(".. data['doorID']..", ".. data['x']..", ".. data['y']..", ".. data['z']..", "
            .. data['yaw']..")")
  end
end)

function Editor_SaveSchematics()
  File_SaveJSONTable('schematics.json', EditorSchematics)
end

local function Editor_LoadSchematics()
  local _data = File_LoadJSONTable('schematics.json')
  if _data ~= nil then
    EditorSchematics = _data
    print('Server: Schematics loaded!')
  else
    print('Server: No schematics.json found in root server directory, one will be made next time a schematic is saved.')
  end
end
AddEvent('OnPackageStart', Editor_LoadSchematics)
